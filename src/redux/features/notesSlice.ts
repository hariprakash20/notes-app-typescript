import { createSlice, PayloadAction } from '@reduxjs/toolkit';
export type RawNote = {
    id: string;
  } & RawNoteData;
  
  export type RawNoteData = {
    title: string;
    markdown: string;
    tagIds: string[];
  };

const initialState: RawNote[] = [{
    id: 'a',
    title: "First Note",
    markdown: 'Hi from the first note',
    tagIds: ['1','2']
}]

export const notesSlice = createSlice({
    name:'notes',
    initialState,
    reducers:{
        // onCreateNote({ tags, ...data }: NoteData): void {
        //     setNotes((prevNotes) => {
        //       return [
        //         ...prevNotes,
        //         { ...data, id: uuidV4(), tagIds: tags.map((tag) => tag.id) },
        //       ];
        //     });
        //   }
        // onCreateNote({})
        

    }
})

export const { } = notesSlice.actions

export default notesSlice.reducer;