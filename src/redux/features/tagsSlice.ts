import { createSlice, PayloadAction} from '@reduxjs/toolkit';
export type Tag = {
    id: string;
    label: string;
  };

const initialState: Tag[]  = [{id: '1', label:'tag1'},{ id: '2', label: 'tag2'}];

export const tagsSlice = createSlice({
    name:'tags',
    initialState,
    reducers:{

    }
})

export default tagsSlice.reducer;